package com.example.recycle_slider

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IntroSlide (
    @Expose
    @SerializedName("home")
    val home : List<Home>
)
    data class Home(

        @SerializedName("name")
        @Expose
        var slide_name:String,

        @SerializedName("name")
        @Expose
        var slide_dec:String,

        @SerializedName("image")
        @Expose
        var slide_image:String
    )



