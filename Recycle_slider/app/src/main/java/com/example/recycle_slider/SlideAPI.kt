package com.example.recycle_slider


import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.*
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.gson.GsonConverterFactory.create
import retrofit2.http.GET

interface SlideApi{
    @GET("home")
    fun getSlide(): Observable<IntroSlide>

    companion object Factory{
        fun create():SlideApi{
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://demo8588158.mockable.io/")
                .build()
            return retrofit.create(SlideApi::class.java)
        }
    }
}