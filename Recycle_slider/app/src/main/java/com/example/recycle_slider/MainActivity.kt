package com.example.recycle_slider

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.viewpager2.widget.ViewPager2
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        callApi()
    }

     fun callApi(){
    val apiService = SlideApi.create()
        apiService.getSlide()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ result ->
                Log.i("result", result.toString())
                rendercountry(result.home)
            }, { error ->
                error.printStackTrace()
            })
    }

    fun rendercountry(slideList: List<Home>) {
        val view = findViewById(R.id.viewpager) as ViewPager2
        view.adapter = SliderAdapter(slideList)
    }
}