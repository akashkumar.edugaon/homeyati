package com.example.recycle_slider

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.slider_list_itam.view.*

class SliderAdapter(private val introSlide: List<Home>): RecyclerView.Adapter<SliderAdapter.IntroSlideViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroSlideViewHolder {
        return IntroSlideViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.slider_list_itam,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        return introSlide.size
    }
    override fun onBindViewHolder(holder: IntroSlideViewHolder, position: Int) {
        val slide= introSlide[position]

        holder.view.home_name.text = slide.slide_name
        holder.view.home_text.text = slide.slide_dec

        Glide.with(holder.view.context)
            .load(slide.slide_image)
            .into(holder.view.slideImage_imageView)
    }

    class IntroSlideViewHolder( val view: View) : RecyclerView.ViewHolder(view)
}